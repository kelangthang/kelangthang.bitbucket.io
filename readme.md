With Ruby setup you can install Jekyll by running the following in your terminal:

gem install jekyll bundler


jekyll build - Builds the site and outputs a static site to a directory called _site.
jekyll serve - Does the same thing except it rebuilds any time you make a change and runs a local web server at http://localhost:4000.
When you’re developing a site you’ll use jekyll serve as it updates with any changes you make.

Run jekyll serve and go to http://localhost:4000 in your browser. You should see “Hello World!”.
